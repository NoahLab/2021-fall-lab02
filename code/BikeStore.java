// Noah Labrecque
// 1931815
package code;

public class BikeStore {
	public static void main(String args[]) {
		Bicycle[] bikes = new Bicycle[4];
		bikes[0] = new Bicycle("Bikes r us", 10, 4.56);
		bikes[1] = new Bicycle("Mountain Racr", 12, 5.26);
		bikes[2] = new Bicycle("Bikes inporium", 14, 2.54);
		bikes[3] = new Bicycle("Cheap bikes", 4, 1.11);

		for (Bicycle bike : bikes) {
			System.out.println(bike);
		}
	}
}
