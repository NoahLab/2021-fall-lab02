// Noah Labrecque
// 1931815
package code;

class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;

	public String getmanufacturer() {
		return manufacturer;
	}

	public int getnumberGears() {
		return numberGears;
	}

	public double getmaxSpeed() {
		return maxSpeed;
	}

	public String toString() {
		return "Manufacturer: " + manufacturer + "\n" + "Gears: " + numberGears + "\n" + "Max Speed: " + maxSpeed + "\n";
	}

	Bicycle(String manufacturer, int numGears, double maxSpeed) {
		this.manufacturer = manufacturer;
		this.numberGears = numGears;
		this.maxSpeed = maxSpeed;
	}
}